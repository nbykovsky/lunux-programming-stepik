#include <stdio.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>

void read_and_report(int *fd, int * counter){
    char buf[100];
    int bytes = read(*fd, buf, 100);
  
    if(bytes == 0){
            *fd = -1;
            return (void)1;
    }
  
    *counter = (*counter)+atoi(buf);

    buf[bytes] = 0;
    
}

int main(){
    int f1 = open("./in1", O_NONBLOCK|O_RDONLY);
    int f2 = open("./in2", O_NONBLOCK|O_RDONLY);

    if(!(f1&&f2)){
        printf("Errorr with opening pipes\n");
        return 0;
    }


    fd_set read_set;
    int counter=0;

    while(f1 > 0 || f2 > 0){
        FD_ZERO(&read_set);
        if(f1 > 0)
            FD_SET(f1, &read_set);
        if(f2 > 0)
            FD_SET(f2, &read_set);

        int result = select(f2+1, &read_set, NULL, NULL, NULL);

        if(result){
          if(f1 >0 && FD_ISSET(f1, &read_set) )
            read_and_report(&f1, &counter);
          if(f2 >0 && FD_ISSET(f2, &read_set) )
            read_and_report(&f2, &counter);
        }

    }
    printf("%d\n", counter);

    return 0;
}
