#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<stdlib.h>

int main(int argc, char ** argv){
    
    if(argc < 3){
        printf("Not enougth arguments\n");
        exit(1);
    }
    int reg1 = shmget(atoi(argv[1]), 1000, 0666|IPC_CREAT);
    int reg2 = shmget(atoi(argv[2]), 1000, 0666|IPC_CREAT);


    int *addr1 = (int *)shmat(reg1, NULL, 0);
    int *addr2 = (int *)shmat(reg2, NULL, 0);

    key_t keyt = ftok("/var", 'c');

    int shm_id = shmget(keyt, 1000, 0666 | IPC_CREAT);
    int *addrt = (int *)shmat(shm_id, NULL, 0);
    
    int i = 0;
    while(i<100){
        addrt[i] = addr1[i] + addr2[i];
        i++;
    }
    printf("%d\n", keyt);

    shmdt((void*)addr1);
    shmdt((void*)addr2);
    shmdt((void*)addrt);

    return 0;
}
