#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){
    char * name = argv[1];
    char * param = argv[2];
    char buf[100];
    sprintf(buf, "%s %s", name, param);

    FILE * pipe = popen(buf, "r");
    int c;
    int counter;
    do{
        c=fgetc(pipe);
        if(c == '0') counter++;
    }while(c != EOF);
    printf("%d\n", counter);

    return 0;
}
