#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void sigh(int sig){
        static int i1=0;
        static int i2=0;
        if(sig == SIGUSR1) i1++;
        else if (sig == SIGUSR2) i2++;
        else {
                printf("%d %d\n", i1, i2);
                exit(0);
        } 
}

int main(){
    int i1=0;
    int i2=0;
    signal(SIGUSR1, sigh);
    signal(SIGUSR2, sigh);
    signal(SIGTERM, sigh);
    //printf("PID=%d\n",(int)getpid());    
    while(1==1){
        usleep(10000);
    }
    return 0;

}
