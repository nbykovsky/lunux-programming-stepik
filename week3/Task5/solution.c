#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(){
    
    pid_t selfpid = fork();
    if(selfpid!=0){
        return 0;
    }
    printf("%d\n", getpid());
    daemon(0,0);
    fclose(stdout);
    sleep(1000);
    return 0;
}
