#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>


char * get_str(const char *str, int position){
    char * token = strtok(str, " ");
    if(position == 0) {
        return token;
    }
    int i=1;
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }
    return token;
}


int main(){
    int fd = open("/proc/self/stat", O_RDONLY);
    char stat_buf[1024];
    read(fd, stat_buf , 1024);
    printf("%s\n", get_str(stat_buf, 4));
    close(fd);
    return 0;
}
