#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>


char * get_str(char *str, int position){
    //printf("%s\n", str);
    char *token; 
    token = strtok(str, " ");
    int i=1;
    if(position == 0) {
        return token;
    }   
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }   
    return token;
}

char * get_str1(char *str, int position){
    char *token;
    int i=1;
    if(position == 0) {
        return token;
    }
    while((token = strsep(&str," "))){
        if(i == position) {
            return token;
        } else if(i > 10000) {
            return NULL;
        }
        i++;
    }
}

char * get_parent(const char * proc){
    char path[100];
    int n = sprintf(path, "/proc/%s/stat", proc);
    int fd = open(path, O_RDONLY);    
    char stat_buf[1024];
    char * parent;
    read(fd, stat_buf , 1024);
    parent = get_str(stat_buf, 4);
    close(fd);
    //printf("%s\n",parent1);
    return parent;
}


int main(int argc, char ** argv){
    if(argc < 2){
        printf("Please enter PID\n");
    }
    char *parent=argv[1];
    printf("%s\n", parent);
    while(strcmp(parent, "1")!=0){
        parent = get_parent(parent);
        printf("%s\n",parent);
    }
    return 0;
}
