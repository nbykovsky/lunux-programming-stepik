#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdbool.h>
#include <dirent.h>
#include <ctype.h>
#include <unistd.h>


char * get_str(char *str, int position){
    char *token; 
    token = strtok(str, " ");
    int i=1;
    if(position == 0) {
        return token;
    }   
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }   
    return token;
}

char * get_parent(const char * proc){
    char path[100];
    int n = sprintf(path, "/proc/%s/stat", proc);
    int fd = open(path, O_RDONLY);        
    char stat_buf[1024];
    char * parent;
    read(fd, stat_buf , 1024);
    parent = get_str(stat_buf, 4); 
    close(fd);
    return parent;
}

int is_child(const char *proc, const char *master){
    if(strcmp(proc, master) == 0){
        return 1;
    }
    char * parent = proc;
    while(parent!=NULL&&strcmp(parent, "0")!=0){
        parent = get_parent(parent);
        if(parent!=NULL&&strcmp(parent, master)==0){
            return 1;
        }
    }
    return 0;

}

bool my_is_number(const char * str){
    int j = 0;
    while(j < strlen(str)){
        if(isdigit(str[j]) == 0){
                return false;
        }
        j++;
    }
    return true;
}

int main(int argc, char ** argv){
    DIR *dir = opendir("/proc");
    char * master = argv[1];
    struct dirent *entry;
    int counter = 0;
    while((entry=readdir(dir))!=NULL){
        char * str = entry->d_name;
        if(my_is_number(str)){
               counter = counter + is_child(str, master);
         }   
    }
    printf("%d\n", counter);
    closedir(dir); 
    return 0;
}
