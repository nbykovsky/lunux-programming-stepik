#include <stdio.h>
#include <dirent.h>
#include <stdbool.h>
#include <dirent.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

bool my_is_number(const char * str){
    int j = 0;
    while(j < strlen(str)){
        if(isdigit(str[j]) == 0){
                return false;
        }
        j++;
    }
    return true;
}

int main(){
    DIR *dir = opendir("/proc");
    struct dirent *entry;
    int counter = 0;
    while((entry=readdir(dir))!=NULL){
        char * str = entry->d_name;
        if(my_is_number(str)){
               char path[100];
               char name[100];
               sprintf(path, "/proc/%s/comm",str);               
               int fd = open(path, O_RDONLY);
               read(fd, name, 100);
               if(strstr(name, "genenv")!=NULL){
                   counter++;
               }
               close(fd);
         }
    }
    printf("%d\n", counter);
    closedir(dir);    
    return 0;

}
