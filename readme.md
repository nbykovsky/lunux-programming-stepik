# Course notes 

## Week 1 

### Compile C++ program  

```bash
gcc -o program program.c
g++ -o program program.cpp 
```

### Dynamic library
```
gcc -o libHello.so -shared -fPIC hello.c
```

### Compile program with dynamic library
```
gcc main.c -L. -lHello -o hello
```
* -L. - tells to look for dynamic libraries in the current folder
* -lHello - tells to use libHello.so dynamic library 

### Byte code
```bash
nm libHello.so
```

### Export name in dynamic library
C++ compiler changes function's name in .so file. To fix that add the following export

hello.h
```C++
#ifndef __HELLO__
#define __HELLO__

extern "C" void hello_message(const char *name);

#endif

```
hello.c
```
#include "hello.h"
#include <stdio.h>

void hello_message(const char *name){
	printf("Hello %s!\n", name);
}
```

### Makefile
```
all: hello libHello.so
	gcc ....

ligHello.so: hello.h hello.c
	gcc -o libHello.so -shared -fPIC hello.c

clean: 
	rm hello
	rm libHello.sh
	rm *.o	

```
[Make utility](http://www.math.tau.ac.il/~danha/courses/software1/make-intro.html)

### Runtime linking

main.c
```C++
#include <stddef.h> // for NULL
#include <stdbool.h>  //for false and true constants
#include <stdio.h>
#include <dlfcn.h>

void (*hello_message)(const char *);

bool init_library(){
	
	void *hdl = dlopen("./libHello.so", RTLD_LAZY);
	if (NULL == hdl)
		return false;
	hello_message = (void (*)(const char*))dlsym(hdl, "hello_message");

	if (NULL == hdl)
		return false;

	return true;

}

int main() {
	if (init_library())
		hello_message("Vasya");
	else
		printf("Library wasn't loaded \n");

	return 0;
}
```

Makefile
```
all: exe lib

exe:	hello.h main.c lib
	gcc main.c -fPIC -ldl -o hello

lib:	hello.h hello.c
	gcc -shared hello.c -fPIC -o libHello.so

clean:
	-rm hello libHello.so 2>/dev/null
```

### [Task 2](https://stepik.org/lesson/26304/step/5?unit=8182)

solution.c
```C++
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <dlfcn.h>


int (*func)(int);

bool init_library(const char * path, const char * func_name){
    void *hdl = dlopen(path, RTLD_LAZY);
    if (NULL == hdl){
            printf("error1: %s \n", path);
            return false;
    }
    func = (void (*)(const char*))dlsym(hdl, func_name);
    if (NULL == hdl){
            printf("error2 \n");
            return false;
    }
}

int main(int argc, char *argv[]){
    if (argc < 3){
        printf("Not enougth args");
        return 1;
    }
    char* path = argv[1];
    char* func_name = argv[2];
    int arg = atoi(argv[3]);

    if (init_library(path, func_name)){
        printf("%d\n", func(arg));    
    }else {
        printf("lib not loaded \n");
    }

    return 0;
}
```  

Makefile
```
all:
	gcc solution.c -fPIC -Wl,-rpath . -o solution -ldl 
```

## Week 2

### Files
operations for working with files
```
fid = open(name, flags);
write(fid, ...);
read(fid, ...);
close(fid);
```
all system calls are discribed in 2nd man section
```
man 2 open
```
open flags
* O_RDONLY - read only
* O_WRONLY - write only
* O_RDWR - read write

lseek - moves pointer on several bites (related to files)  
fcntl - different operations on files  
creat - creates file  
unlink - removes file  
details in man 2

### Folders
functions for working with folders
```
DIR * opendir(const char *name);
DIR * fdopendir(int fd);
int closedir(DIR *);
```
description of DIR in dirend.h

traversing folders tree
```
DIR *dir;
struct dirent *entry;
while((entry=readdir(dir))!=NULL) {
//actions on dirent
}
```

### Links 
create file
```
touch file
echo Hello > file
```
information about file
```
stat test
```
hard link
```
ln test test.hardlink
```
soft link
```
ln -s test test.softlink
```
work with links
```C++
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int c, const char **v){
	char buf[100];
	size_t sz=0;

	if(c!=2){
		printf("\nPlease call\n\t%s filename\n", v[0]);
		return 1;	
	}

	//O_NOFOLLOW means don't open links
	int fd=open(v[1], O_RDONLY|O_NOFOLLOW);
	perror("fd");
	printf("fd=%d\n", fd);
	sz=read(fd, buf, 100);

	printf("sz=%ld\n", sz);
}
```
to open symlink file add O_PATH flag

### File systems
dependencies
```
ldd execfile
```
ltrace - to track libraries dependencies  
strace - to track system calls  

procfs - special file system which stores information about all processes and availiable in /proc/... folder  

sys_open(name) - system call which opens any file (returns file descriptor fd)  
sys_write(fd) - system calls which write into file  
sys_read(fd) - reads from file  

all mount points in the system
```
mount
```
id of current process
```
echo $$
```
/proc/PID/comm - name of current process  
/proc/PID/cmdline - the same  

pwd - current path
current folder of process
```
ls -la /proc/PID/cwd
```
environ - (file in /proc/PID) all env variables 
limits - limits
ns - namespaces
stat - statistics
status - readable statistics

information about processers
```
cat /proc/cpuinfo |less
```

### Debugging GDB

to include debug info into exec file
```
gcc -g3 ....
```

to turn of optimization
```
gcc -o0
```

starting GDB
```bash
gdb ./executable
gdb ./executable -c core
gdb ./executable -pid process-id
gdb -p process-id
```

gdb commands
```
help <command>
info args
info breakpoints
info watchpoints
info registers
info threads
info signals
where
```

gdb execution commands
```bash
r/run
r/run arguments
c/continue
continue number -cont. ignore break
finish - continue to the end of function
kill
q/quit
```

line execution
* step (into a function)
* next (next line of code)
* until line-number
* stepi/nexti step for assempler instruction


break and watch
* break function/line
* break <+/-> <numOfLines>
* break filename:line or filename:function
* break *instruction_address
* break ... if condition
* break line thread tid
* clear
* enable/disable
* watch condition

stack inspection
* bt/backtrace
* f/frame [number]
* backtrace full
* up/down number
* info frame

variables and sources
* list [+n | -n]
* set listsize num
* list start, end
* p/print variable
* p/[format] variable (x,o,d,f,c - format)

Turn off limits for core dumps
```bash
ulimit -c unlimited
gcc -g3 bug.cpp
gdb -c core ./a.out
```
gdb commands
```
gdb ./a.out - run gdb
b print - add break point on print function (could be any function)
info breakpoints - outputs break points
r - run program
p/d v - output v variable in decimal format
list - output source code
list -15 - move output window to 15 rows back
b 6 - add break point on row 6
kill - terminate app
fr 1 - move to first frame
help - help

```

## Week 3

### Processes life cycle
init - main process with PID = 1  
process tree
```
pstree -p [PID]
```

process creation
* fork/exec
* clone
fork (man fork for info) - creates exact copy of the current process (PID, PPID, blockings, list of signals, timers will be different). fork returns new PID in parent process and 0 in child pricess, this property is used for differentiation new and old process 
exec (man exec for info) - replace image of the current process. System call calls execve  

assume we have /usr/bin/parent which should execute /usr/bin/child
```
pid_t pid = fork();
if (pid == 0) {
	exec('/usr/bin/child', ...)
	exit(0)
} else {
	//wait child (details in man wait)
	waitpid(pid);
}
```

### Processes atributes (system call clone)
Clone could copy process more flexable then fork  

Process context
* Open files
* File systems (mount)
* IO operations
* Network environment
* Processes ids
* memory
* registers
* hostname 

to controll context use flags arg of clone func (man 2 clone for details)

example
```C++
#include <sched.h> //for clone
#include <stdio.h>
#include <unistd.h> //for usleep

#define STACK_SIZE 10000
//stack grows from end to start
char child_stack[STACK_SIZE+1];

int child(void *params){
	int c=0;
	while(true){
		usleep(50000);
		printf("child turn %d\n", c++);
	}
}

int main(){
	int c=0;
	//end of stack (child_stack+STACK_SIZE)
	int result=clone(child, child_stack+STACK_SIZE,0,0);
	// int result=clone(child, child_stack+STACK_SIZE,CLONE_PARENT,0); - example of flag usage
	printf("clone result = %d\n", result);

	while(true){
		usleep(50000);
		printf("parent turn %d\n", c++);
	}
}
```
### Processed daemons
to create daemon
1. Create the new process using fork
2. Change to root folder (/) (to don't block FS)
3. Unattach process from the current session. Assing the new session id (setsid(2) - man 2 setsid)
4. Close stdIO files. close(stdin), close(stdout), close(stderr)

### [Task 1](https://stepik.org/lesson/27846/step/2?unit=9092)
```
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>


char * get_str(const char *str, int position){
    char * token = strtok(str, " ");
    if(position == 0) {
        return token;
    }   
    int i=1;
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }   
    return token;
}


int main(){
    int fd = open("/proc/self/stat", O_RDONLY);
    char stat_buf[1024];
    read(fd, stat_buf , 1024);
    printf("%s\n", get_str(stat_buf, 4));
    close(fd);
    return 0;
}

```

### [Task 2](https://stepik.org/lesson/27846/step/3?unit=9092)
```C++
#include <stdio.h>
#include <dirent.h>
#include <stdbool.h>
#include <dirent.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

bool my_is_number(const char * str){
    int j = 0;
    while(j < strlen(str)){
        if(isdigit(str[j]) == 0){ 
                return false;
        }
        j++;
    }   
    return true;
}

int main(){
    DIR *dir = opendir("/proc");
    struct dirent *entry;
    int counter = 0;
    while((entry=readdir(dir))!=NULL){
        char * str = entry->d_name;
        if(my_is_number(str)){
               char path[100];
               char name[100];
               sprintf(path, "/proc/%s/comm",str);    
               int fd = open(path, O_RDONLY);
               read(fd, name, 100);
               if(strstr(name, "genenv")!=NULL){
                   counter++;
               }   
               close(fd);
         }   
    }
    printf("%d\n", counter);
    closedir(dir); 
    return 0;

}

```

### [Task 3](https://stepik.org/lesson/27846/step/4?unit=9092)
```C++
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>


char * get_str(char *str, int position){
    char *token; 
    token = strtok(str, " ");
    int i=1;
    if(position == 0) {
        return token;
    }   
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }   
    return token;
}

char * get_parent(const char * proc){
    char path[100];
    int n = sprintf(path, "/proc/%s/stat", proc);
    int fd = open(path, O_RDONLY);        
    char stat_buf[1024];
    char * parent;
    read(fd, stat_buf , 1024);
    parent = get_str(stat_buf, 4); 
    close(fd);
    return parent;
}

int main(int argc, char ** argv){
    if(argc < 2){ 
        printf("Please enter PID\n");
    }   
    char *parent=argv[1];
    printf("%s\n", parent);
    while(strcmp(parent, "1")!=0){
        parent = get_parent(parent);
        printf("%s\n",parent);
    }   
    return 0;
}

```

### [Task 4](https://stepik.org/lesson/27846/step/5?unit=9092)
```C++
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdbool.h>
#include <dirent.h>
#include <ctype.h>
#include <unistd.h>


char * get_str(char *str, int position){
    char *token; 
    token = strtok(str, " ");
    int i=1;
    if(position == 0) {
        return token;
    }   
    while(i!=position){
        token=strtok(NULL, " ");
        i++;
    }   
    return token;
}

char * get_parent(const char * proc){
    char path[100];
    int n = sprintf(path, "/proc/%s/stat", proc);
    int fd = open(path, O_RDONLY);
    char stat_buf[1024];
    char * parent;
    read(fd, stat_buf , 1024); 
    parent = get_str(stat_buf, 4);
    close(fd);
    return parent;
}

int is_child(const char *proc, const char *master){
    if(strcmp(proc, master) == 0){ 
        return 1;
    }   
    char * parent = proc;
    while(parent!=NULL&&strcmp(parent, "0")!=0){
        parent = get_parent(parent);
        if(parent!=NULL&&strcmp(parent, master)==0){
            return 1;
        }   
    }
    return 0;

}

bool my_is_number(const char * str){
    int j = 0;
    while(j < strlen(str)){
        if(isdigit(str[j]) == 0){
                return false;
        }
        j++;
    }
    return true;
}

int main(int argc, char ** argv){
    DIR *dir = opendir("/proc");
    char * master = argv[1];
    struct dirent *entry;
    int counter = 0;
    while((entry=readdir(dir))!=NULL){
        char * str = entry->d_name;
        if(my_is_number(str)){
               counter = counter + is_child(str, master);
         }  
    }
    printf("%d\n", counter);
    closedir(dir);
    return 0;
}

```

### [Task 5](https://stepik.org/lesson/27846/step/6?unit=9092)
```C++
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(){
        
    pid_t selfpid = fork();
    if(selfpid!=0){
        return 0;
    }   
    printf("%d\n", getpid());
    daemon(0,0);
    fclose(stdout);
    sleep(1000);
    return 0;
}

```

## Week 4  
### Inter Process Communication (IPC)
* Files
* Named(Unnamed) Pipes
* Signals
* Shared memory
* Message queue (mq_open, mq_send etc)
* Shared memory
* Sockets
* Posix 
* rpc (remote procedure call)
* IPC(system v)

### Pipes
![Slide](./pics/week4_1.png "Slide1")
#### Non Named Pipes
pipe(2) creates non named pipe (look at example in man)  
popen(3) creates file from cmd command (using non named pipe)  

#### Named Pipes
mkfifo(3) - creates named pipe  

### Signals
Signal #9 - hard termination of the process  
singlans are defined in <signal.h>  
* SIGINT 2 - terminates program
* SIGABRT 6 - terminates abort
* SIGTERM 15 - gently asks program to stop
* SIGKILL 9 - kills application (cannot be redefined)
* SIGCHLD 17 - sends after termination of child process

#### Functions for working with signals
* kill(1) - sends signal to process (cli)
* signal - defines a logic of processing signal 

list of signals

```bash
kill -l
```
* kill(2) - c function which sends signal
* signal(2) - c function which defines the logic

SIGSTOP, SIGKILL cannot be redefined  

example which redefines signals
```C++
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void mysignal_handler(int signalno){
	printf("Called with %d\n", signalno);
}

int main(){
	int c=0;
	
	signal(SIGINT, mysignal_handler);

	while(1){
		printf("Hello\n");
		usleep(500000);
		return 0;
	}
}
```
to test call ctrl+C while running

### Shared memory
![Slide](./pics/week4_2.png "Slide1")

#### Functions for working with shared memory
* shmget(2) - creates shared memory region
* shmat - connect to the existing shared memory region
* shmdt - shared memory deattach
* shmctl - controls different parameters

#### shmget(2)
* key - key which both application should know to share the memory. If IPC_PRIVATE, memory will be used by only one application
* flag IPC_CREAT/IPC_EXCL - works like flags for files
* SHM_NORESERVE - makes OS don't reserve phisical memory

information about shared memory regions 
```bash
ipcs -m
```
## I/O multiplexing (async usage)
process gets blocked when reads from empty named pipe  
the following macros are used to overcome blocking problem
![Slide](./pics/week4_3.png "Slide1")

example
```C++
#include <stdio.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>

void read_and_report(int fd){
	char buf[100];
	
	printf("FD %d is ready to read\n", fd);
	int bytes = read(fd, buf, 100);

	buf[bytes] = 0;
	printf("Get %d bytes from %d : %s\n", bytes, fd, buf);
}

int main(){
	int f1 = open("./f1.fifo", O_RDWR);
	int f2 = open("./f2.fifo", O_RDWR);

	if(!(f1&&f2)){
		printf("Errorr with opening pipes\n");
		return 0;
	}

	printf("Descriptors: %d %d\n",f1,f2);

	fd_set read_set;
	
	while(true){
		FD_ZERO(&read_set);
		FD_SET(f1, &read_set);
		FD_SET(f2, &read_set);

		int result = select(f2+1, &read_set, NULL, NULL, NULL);

		if(result){
		  if( FD_ISSET(f1, &read_set) )
		    read_and_report(f1);
		  if( FD_ISSET(f2, &read_set) )
		    read_and_report(f2);
		}
	}

	return 0;

}
```
to test the code run
```bash
mkfifo f1.fifo
mkfifo f2.fifo
./run
echo "hello" > f1.fifo
echo "hello" > f2.fifo
```
program above has mistake redarding working with files

### [Task 1](https://stepik.org/lesson/28341/step/1?unit=9513)
```C++
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){
    char * name = argv[1];
    char * param = argv[2];
    char buf[100];
    sprintf(buf, "%s %s", name, param);

    FILE * pipe = popen(buf, "r");
    int c;
    int counter;
    do{ 
        c=fgetc(pipe);
        if(c == '0') counter++;
    }while(c != EOF);
    printf("%d\n", counter);

    return 0;
}

```

### [Task 2](https://stepik.org/lesson/28341/step/2?unit=9513)
```C++
#include <stdio.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>

void read_and_report(int *fd, int * counter){
    char buf[100];
    int bytes = read(*fd, buf, 100);
  
    if(bytes == 0){ 
            *fd = -1; 
            return (void)1;
    }   
  
    *counter = (*counter)+atoi(buf);

    buf[bytes] = 0;
        
}

int main(){
    int f1 = open("./in1", O_NONBLOCK|O_RDONLY);
    int f2 = open("./in2", O_NONBLOCK|O_RDONLY);

    if(!(f1&&f2)){
        printf("Errorr with opening pipes\n");
        return 0;
    }   


    fd_set read_set;
    int counter=0;

    while(f1 > 0 || f2 > 0){ 
        FD_ZERO(&read_set);
        if(f1 > 0)
            FD_SET(f1, &read_set);
        if(f2 > 0)
            FD_SET(f2, &read_set);

        int result = select(f2+1, &read_set, NULL, NULL, NULL);

        if(result){
          if(f1 >0 && FD_ISSET(f1, &read_set) )
            read_and_report(&f1, &counter);
          if(f2 >0 && FD_ISSET(f2, &read_set) )
            read_and_report(&f2, &counter);
        }   

    }
    printf("%d\n", counter);

    return 0;

```

### [Task 3](https://stepik.org/lesson/28341/step/3?unit=9513)
```C++
#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void sigh(int sig){
        static int i1=0;
        static int i2=0;
        if(sig == SIGUSR1) i1++;
        else if (sig == SIGUSR2) i2++;
        else {
                printf("%d %d\n", i1, i2);
                exit(0);
        }
}

int main(){
    int i1=0;
    int i2=0;
    signal(SIGUSR1, sigh);
    signal(SIGUSR2, sigh);
    signal(SIGTERM, sigh);
    //printf("PID=%d\n",(int)getpid());    
    while(1==1){
        usleep(10000);
    }   
    return 0;

}

```

### [Task 4](https://stepik.org/lesson/28341/step/4?unit=9513)
```C++
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>

void sigh(int sig){
    exit(0);
}

int main(){

    pid_t selfpid = fork();
    if(selfpid!=0){
        return 0;
    }   
    printf("%d\n", getpid());
    daemon(0,0);
    fclose(stdout);
    signal(SIGURG, sigh);
    while(true){
       int x=1; 
    }   
    return 0;
}

```

### [Task 5](https://stepik.org/lesson/28341/step/5?unit=9513)
```C++
#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<stdlib.h> 
   
int main(int argc, char ** argv){
        
    if(argc < 3){
        printf("Not enougth arguments\n");
        exit(1);
    }
    int reg1 = shmget(atoi(argv[1]), 1000, 0666|IPC_CREAT);
    int reg2 = shmget(atoi(argv[2]), 1000, 0666|IPC_CREAT);
    
    
    int *addr1 = (int *)shmat(reg1, NULL, 0);
    int *addr2 = (int *)shmat(reg2, NULL, 0);
    
    key_t keyt = ftok("/var", 'c'); 
    
    int shm_id = shmget(keyt, 1000, 0666 | IPC_CREAT);
    int *addrt = (int *)shmat(shm_id, NULL, 0);
    
    int i = 0;
    while(i<100){
        addrt[i] = addr1[i] + addr2[i];
        i++;
    }
    printf("%d\n", keyt);
    
    shmdt((void*)addr1);
    shmdt((void*)addr2);
    shmdt((void*)addrt);

    return 0;
}
```

## Week 5

### Classification of network levels
* OSI
* TCP/IP

![](pics/week5_1.png)

![](pics/week5_2.png)

![](pics/week5_3.png)

![](pics/week5_4.png)

### Sockets

![](pics/week5_5.png)
* UDP - sends packages
* TCP - works with streams

![](pics/week5_6.png)

![](pics/week5_7.png)

### Program which resolves domain names
gethostbyname(3) - useful function

resolv.c
```C++
#include <netdb.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int main(int c, char **v){
	if (c!=2)
		return -1;
	struct hostent *h;
	h = gethostbyname(v[1]);
	
	if(NULL==h){
		printf("Error\n");
		return -1;
	}

	printf("Canonical name %s\n", h->h_name);
	printf("Type = %s len=%d\n", (h->h_addrtype == AF_INET)?"ipv4":"ipv6", h->h_length);

	int i=0;
	while(NULL !=h->h_addr_list[i]) {
		struct in_addr *a = (struct in_addr*)h->h_addr_list[i];
		printf("%s\n", inet_ntoa(*a));
		i++;
	}
}
```
useful man pages
* inet(3)
* ip(7) - socket structure 

### Sockets datagram

useful utilities
* netstat - shows open sockets
* nc - creates clinets and servers

Server TCP
```bash
nc 300 -l
```

Client TCP
```bash
nc licalhost 3000
nc licalhost 3000 < data
```

Server UDP
```bash
nc localhost -u 3000 -l
```

Client UDP
```bash
nc localhost 3000 -u
```

example (UDP server)
```C++
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

struct sockaddr_in local;

int main (int c, char **v){
	int s = socket(AF_INET, SOCK_DGRAM, 0);
	printf("socket = %d\n",s);
	
	inet_aton("127.0.0.1", &local.sin_addr);
	local.sin_port = htons(1234);
	local.sin_family = AF_INET;

	int result = bind(s, (struct sockaddr*) &local, sizeof(local));
	printf("%d\n", result);

	char buf[BUFSIZ];
	read(s, buf, BUFSIZ);
	printf("%s, bue\n", buf);
	
	return 0;
	
}
```
to test
```
nc 127.0.0.1 -u 1234
```

example (TCP client + server)
```C++
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>

struct sockaddr_in local;

int server(){
    int ss=socket(AF_INET, SOCK_STREAM, 0);
    int cs;

    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(1234);
    local.sin_family = AF_INET;

    bind(ss, (struct sockaddr*) &local, sizeof(local));

    //5 means number of cuncurrent clients
    listen(ss, 5);

    cs=accept(ss, NULL, NULL);

    char buf[BUFSIZ];
    read(cs, buf, BUFSIZ);
    printf("%s\n", buf);
    close(cs);
    return 0;
}

int client(){
    int s = socket(AF_INET, SOCK_STREAM, 0);

    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(1234);
    local.sin_family = AF_INET;

    connect(s, (struct sockaddr*) &local, sizeof(local));

    char buf[BUFSIZ]="Hello\n";
    write(s, buf, strlen(buf)+1);
    close(s);
    return 0;
}

int main(int c, char **v){

    if(c!=2){
        return printf("Use: %s [s|c]\n", v[0]);
    }

    struct sockaddr_in local;

    if(v[1][0] == 's'){
        server();   
    }

    if(v[1][0] == 'c'){
        client();   
    }

}
```
send(2) behaves like write

### [Task 1](https://stepik.org/lesson/28503/step/1?unit=9626)
```C++
#include <netdb.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int main(int c, char **v){
    if (c!=2)
        return -1; 
    struct hostent *h; 
    h = gethostbyname(v[1]);
       
    if(NULL==h){
        printf("Error\n");
        return -1; 
    }   

    int i=0;
    while(NULL !=h->h_addr_list[i]) {
        struct in_addr *a = (struct in_addr*)h->h_addr_list[i];
        printf("%s\n", inet_ntoa(*a));
        i++;
    }
}
```

### [Task 2](https://stepik.org/lesson/28503/step/2?unit=9626)
```C++
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>

struct sockaddr_in local;

int main (int c, char **v){
    int s = socket(AF_INET, SOCK_DGRAM, 0);

    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(atoi(v[1]));
    local.sin_family = AF_INET;

    int result = bind(s, (struct sockaddr*) &local, sizeof(local));

    char buf[BUFSIZ];
    while(true){
        memset(buf, 0, BUFSIZ);
        read(s, buf, BUFSIZ);
        if(strncmp(buf, "OFF\n", 4) == 0) break;
        printf("%s\n", buf);
    }
    return 0;

}

```

### [Task 3](https://stepik.org/lesson/28503/step/3?unit=9626)
```C++
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdbool.h>



struct sockaddr_in local;

int cmp(const void * x1, const void * x2){ 
  return ( *(char*)x2 - *(char*)x1 );    
}

int server(int port){
    int ss=socket(AF_INET, SOCK_STREAM, 0); 
    int cs; 

    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(port);
    local.sin_family = AF_INET;

    bind(ss, (struct sockaddr*) &local, sizeof(local));
    
    listen(ss, 5); 

    cs=accept(ss, NULL, NULL);

    char buf[BUFSIZ];
    while(true){
        memset(buf, 0, BUFSIZ);
        read(cs, buf, BUFSIZ);
        if(strncmp(buf, "OFF", 3) == 0) break;
        qsort(buf, strlen(buf), 1, cmp);
        write(cs, buf, strlen(buf)+1);
    }   
    close(cs);
    return 0;
}

int main(int c, char **v){

    if(c!=2){
        return printf("Use: %s port\n", v[0]);
    }   

    struct sockaddr_in local;
    server(atoi(v[1]));

}
```
