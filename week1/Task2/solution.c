#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <dlfcn.h>


int (*func)(int);

bool init_library(const char * path, const char * func_name){
    void *hdl = dlopen(path, RTLD_LAZY);
    if (NULL == hdl){
            printf("error1: %s \n", path);
            return false;
    }
    func = (void (*)(const char*))dlsym(hdl, func_name);
    if (NULL == hdl){
            printf("error2 \n");
            return false;
    }
}

int main(int argc, char *argv[]){
    if (argc < 3){
        printf("Not enougth args");
        return 1;
    }
    char* path = argv[1];
    char* func_name = argv[2];
    int arg = atoi(argv[3]);    

    if (init_library(path, func_name)){
        printf("%d\n", func(arg));    
    }else {
        printf("lib not loaded \n");
    }

    return 0;
}
