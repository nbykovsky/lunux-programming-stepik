#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdbool.h>



struct sockaddr_in local;

int cmp(const void * x1, const void * x2){ 
  return ( *(char*)x2 - *(char*)x1 );           
}

int server(int port){
    int ss=socket(AF_INET, SOCK_STREAM, 0);
    int cs;

    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(port);
    local.sin_family = AF_INET;

    bind(ss, (struct sockaddr*) &local, sizeof(local));

    listen(ss, 5);

    cs=accept(ss, NULL, NULL);

    char buf[BUFSIZ];
    while(true){
        memset(buf, 0, BUFSIZ);
        read(cs, buf, BUFSIZ);
        if(strncmp(buf, "OFF", 3) == 0) break;
        qsort(buf, strlen(buf), 1, cmp);
        write(cs, buf, strlen(buf)+1);
    }
    close(cs);
    return 0;
}

int main(int c, char **v){

    if(c!=2){
        return printf("Use: %s port\n", v[0]);
    }

    struct sockaddr_in local;
    server(atoi(v[1]));

}
