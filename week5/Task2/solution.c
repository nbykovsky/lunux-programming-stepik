#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>

struct sockaddr_in local;

int main (int c, char **v){
    int s = socket(AF_INET, SOCK_DGRAM, 0);
    
    inet_aton("127.0.0.1", &local.sin_addr);
    local.sin_port = htons(atoi(v[1]));
    local.sin_family = AF_INET;

    int result = bind(s, (struct sockaddr*) &local, sizeof(local));
    
    char buf[BUFSIZ];
    while(true){
        memset(buf, 0, BUFSIZ);
        read(s, buf, BUFSIZ);
        if(strncmp(buf, "OFF\n", 4) == 0) break;
        printf("%s\n", buf);
    }
    return 0;

}
